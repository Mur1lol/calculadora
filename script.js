//VARIAVEIS--------------------------------------------------------
var valorTotal = "";
var valor1 = 0;
var valor2 = 0;

var troca = 0;

var adicao = 0;
var subtracao = 0;
var multiplicacao = 0;
var divisao = 0;

//NUMEROS----------------------------------------------------------
document.getElementById('1').addEventListener('click', function() { 
    valores(1)
})

document.getElementById('2').addEventListener('click', function() { 
    valores(2)
})

document.getElementById('3').addEventListener('click', function() { 
    valores(3)
})

document.getElementById('4').addEventListener('click', function() { 
    valores(4)
})

document.getElementById('5').addEventListener('click', function() { 
    valores(5)
})

document.getElementById('6').addEventListener('click', function() { 
    valores(6)
})

document.getElementById('7').addEventListener('click', function() { 
    valores(7)
})

document.getElementById('8').addEventListener('click', function() { 
    valores(8)
})

document.getElementById('9').addEventListener('click', function() { 
    valores(9)
})

document.getElementById('0').addEventListener('click', function() { 
    valores(0)
})

//OPERAÇÕES--------------------------------------------------------
document.getElementById('+').addEventListener('click', function() { 
	adicao = 1;
    operacoes("+")
})

document.getElementById('-').addEventListener('click', function() { 
	subtracao = 1;
    operacoes("-")
})

document.getElementById('*').addEventListener('click', function() { 
    multiplicacao = 1;
    operacoes("*")
})

document.getElementById('/').addEventListener('click', function() { 
	divisao = 1;
    operacoes("/")
})

//ESPECIAL 
document.getElementById('C').addEventListener('click', function() { 
    valorTotal = "";
    valor1 = 0;
    valor2 = 0;
    troca = 0;
    ativar();
    document.getElementById('painel').value = valorTotal;
})

document.getElementById('=').addEventListener('click', function() { 
    contas();
    valorTotal = valor1;
    valor2= 0;
    ativar();
	document.getElementById('painel').value = valorTotal;
})

//FUNÇÕES----------------------------------------------------------
function contas() {
    if (adicao == 1) {
        valor1 = valor1 + valor2;
        adicao = 0;
    }
    else if (subtracao == 1 && multiplicacao == 0 && divisao == 0) {
        valor1 = valor1 - valor2;
        subtracao = 0;
    }
    else if (subtracao == 0 && multiplicacao == 1) {
        valor1 = valor1 * valor2;
        multiplicacao = 0;
    }
    else if (subtracao == 1 && multiplicacao == 1) {
        valor1 = valor1 * -valor2;
        subtracao = 0;
        multiplicacao = 0;
    }
    else if (subtracao == 0 && divisao == 1) {
        valor1 = valor1 / valor2;
        divisao = 0;
    }
    else if (subtracao == 1 && divisao == 1) {
        valor1 = valor1 / -valor2;
        subtracao = 0;
        divisao = 0;
    }
}

function valores(x) {
    valorTotal = valorTotal + x.toString();

    if(troca == 0 && subtracao == 0) {
        valor1 = parseInt(valor1.toString() + x);
    }
    else if(troca == 0 && subtracao == 1) {
        valor1 = parseInt(valor1.toString() - x);
    }
    else {
        valor2 = parseInt(valor2.toString() + x);
    }

    if(valor2 != 0) {
        desativar()
    }

    document.getElementById('painel').value = valorTotal;
}

function operacoes(x) {
    valorTotal = valorTotal + x;

    if (valor1 != 0) { troca= 1; }

    document.getElementById('painel').value = valorTotal;
}

function ativar() {
    document.getElementById('+').disabled=false;
    document.getElementById('-').disabled=false;
    document.getElementById('*').disabled=false;
    document.getElementById('/').disabled=false;
}

function desativar() {
    document.getElementById('+').disabled=true;
    document.getElementById('-').disabled=true;
    document.getElementById('*').disabled=true;
    document.getElementById('/').disabled=true;
}